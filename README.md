# 100 Balls

Open source clone of 100 Balls for Ubuntu Touch

Based on Bacon2D, a framework to ease 2D game development, providing
ready-to-use QML components useful for creating compeling games.

## Installation

### Ubuntu Touch

To build the click package, install [Clickable](https://clickable-ut.dev/en/latest/install.html).
Simply run from the app directory:

```bash
clickable --arch arm64 # or armhf
```

See [Clickable Documentation](https://clickable-ut.dev/en/latest/) for details.

### On desktop

You need Qt 5.x and [Bacon2D][bacon] installed on your system.
Then you can clone this repository, and launch the game executing

```bash
qmlscene 100balls/100balls.qml
```

from the root of the project.

## Contribution

If you want to help with development, or you have a suggestion, open an issue on
this repository.

## Thanks

This game wouldn't be possible without help of these developers:

- Riccardo Padovani
- Alan Pope
- Ken VanDine
- Nekhelesh Ramananthan
- Stefano Verzegnassi

and with the support of all guys of the Ubuntu Community.

Thanks to all!

[bacon]: https://github.com/Bacon2D/Bacon2D
